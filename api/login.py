import requests
import app


class LoginApi:
    def __init__(self):
        # 登录URL
        self.login_url = app.BASE_URL + "/api/sys/login"

    # 登录
    def login(self, mobile, pwd):
        data = {}
        if mobile is not None:
            data["mobile"] = mobile
        if pwd:
            data["password"] = pwd
        # data = None
        # if flag:
        #     pass
        # else:
        #     data = {}
        #     if mobile is not None:
        #         data["mobile"] = mobile
        #     if pwd:
        #         data["password"] = pwd
        print("data====", data)
        return requests.post(self.login_url, json=data)

    # 登录
    def login2(self):
        return requests.post(self.login_url, json=None)
