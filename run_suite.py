import unittest
import app

from script.test_emp import TestEmp
from script.test_login import TestLogin
from lib.HTMLTestRunner import HTMLTestRunner

suite = unittest.TestSuite()
suite.addTest(unittest.makeSuite(TestLogin))
suite.addTest(unittest.makeSuite(TestEmp))

# unittest.TextTestRunner().run(suite)

# 定义测试报告文件路径
report_file = app.BASE_DIR + "/report/report.html"

with open(report_file, "wb") as f:
    # 实例化运行器对象
    runner = HTMLTestRunner(f, verbosity=2, title="IHRM接口自动化测试报告", description="xxxx")

    # 执行测试套件
    runner.run(suite)
