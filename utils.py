import pymysql
import traceback


# 通用断言方法
def common_assert(test_case, response, status_code, success, code, message):
    test_case.assertEqual(status_code, response.status_code)
    json_data = response.json()
    test_case.assertEqual(success, json_data.get("success"))
    test_case.assertEqual(code, json_data.get("code"))
    test_case.assertIn(message, json_data.get("message"))


# 数据库操作工具类
class DBUtil:
    _conn = None  # 数据库连接对象

    # 获取数据库连接
    @classmethod
    def get_conn(cls):
        if cls._conn is None:
            cls._conn = pymysql.connect("localhost", "root", "root", "books", 3306)
        return cls._conn

    # 关闭数据库连接
    @classmethod
    def close_conn(cls):
        if cls._conn:
            cls._conn.close()
            cls._conn = None

    # 获取游标对象
    @classmethod
    def get_cursor(cls):
        return DBUtil.get_conn().cursor()

    # 关闭游标对象
    @classmethod
    def close_cursor(cls, cursor):
        if cursor:
            cursor.close()

    # 查询一条数据
    @classmethod
    def get_one(cls, sql):
        result = None
        cursor = None
        try:
            cursor = DBUtil.get_cursor()
            cursor.execute(sql)
            result = cursor.fetchone()
        except Exception as e:
            traceback.print_exc()
        finally:
            # DBUtil.close_cursor(cursor)
            if cursor:
                cursor.close()
        return result

    # 新增、删除、修改




if __name__ == '__main__':
    data = DBUtil.get_one("select * from t_book t where t.id=1")
    print("data===", data)
