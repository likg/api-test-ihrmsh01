import json
import unittest

import app
import utils

from api.emp import EmpApi
import logging
from parameterized import parameterized
import pymysql


# 构建测试数据，读取JSON文件
def build_add_emp_data():
    test_data = []
    with open(app.BASE_DIR + "/data/emp.json", encoding="UTF-8") as f:
        json_data = json.load(f)
        case_data = json_data.get("add_emp")

        username = case_data.get("username")
        mobile = case_data.get("mobile")
        workNumber = case_data.get("workNumber")
        status_code = case_data.get("status_code")
        success = case_data.get("success")
        code = case_data.get("code")
        message = case_data.get("message")
        test_data.append((username, mobile, workNumber, status_code, success, code, message))
    logging.info("add emp test_data={}".format(test_data))
    return test_data
class TestEmp(unittest.TestCase):
    emp_id = None  # 员工id
    @classmethod
    def setUpClass(cls) -> None:
        cls.emp_api = EmpApi()

    # 添加员工
    @parameterized.expand(build_add_emp_data)
    def test01_add_emp(self, username, mobile, workNumber, status_code, success, code, message):
        # 测试数据
        # username = "tom01"
        # mobile = "13012980009"
        # workNumber = "E001"

        # 数据清理
        sql = "DELETE FROM bs_user WHERE mobile='{}';".format(mobile)

        # 发送请求
        r = self.emp_api.add_emp(username, mobile, workNumber)
        logging.info("json data={}".format(r.json()))

        # 断言
        utils.common_assert(self, r, status_code, success, code, message)

        # 获取员工id，并保存
        json_data = r.json()
        TestEmp.emp_id = json_data.get("data").get("id")

    # 查询员工
    def test02_get_emp(self):
        # 测试数据
        id = TestEmp.emp_id

        # 发送请求
        r = self.emp_api.get_emp(id)
        logging.info("json data={}".format(r.json()))

        # 断言
        utils.common_assert(self, r, 200, True, 10000, "操作成功")
        username = r.json().get("data").get("username")
        self.assertEqual("tom01", username)

    # 修改员工
    def test03_update_emp(self):
        # 测试数据
        id = TestEmp.emp_id
        username = "tom01-new"

        # 发送请求
        r = self.emp_api.update_emp(id, username)
        logging.info("json data={}".format(r.json()))

        # 断言
        utils.common_assert(self, r, 200, True, 10000, "操作成功")

        # 校验数据库中的用户名
        # 创建数据库连接
        # conn = pymysql.connect("localhost", "root", "root", "books", 3306)
        # # 获取游标对象
        # cursor = conn.cursor()
        # # 执行操作
        # sql = "SELECT t.id,t.username FROM `bs_user` t WHERE t.id='1';"
        # cursor.execute(sql)
        # db_data = cursor.fetchone()
        # logging.info("db_data===={}".format(db_data))
        # db_username = db_data[1]
        # # 关闭游标
        # cursor.close()
        # # 关闭连接
        # conn.close()
        sql = "SELECT t.id,t.username FROM `bs_user` t WHERE t.id='1';"
        db_data = utils.DBUtil.get_one(sql)
        logging.info("db_data===={}".format(db_data))
        db_username = db_data[1]
        self.assertEqual(username, db_username)




    # 删除员工
    def test04_delete_emp(self):
        # 测试数据
        id = TestEmp.emp_id

        # 发送请求
        r = self.emp_api.delete_emp(id)
        logging.info("json data={}".format(r.json()))

        # 断言
        utils.common_assert(self, r, 200, True, 10000, "操作成功")
