import unittest

import app
from api.login import LoginApi
import logging
import utils
import json
from parameterized import parameterized


# 构建测试数据，读取JSON文件
def build_data():
    test_data = []
    with open(app.BASE_DIR + "/data/login.json", encoding="UTF-8") as f:
        json_data = json.load(f)
        for case_data in json_data:
            test_data.append((case_data.get("mobile"), case_data.get("pwd"),
                              case_data.get("status_code"),
                              case_data.get("success"),
                              case_data.get("code"),
                              case_data.get("message")))
    logging.info("login test_data={}".format(test_data))
    return test_data


class TestLogin(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.login_api = LoginApi()

    def test_02(self):
        pass

    # 登录成功
    @parameterized.expand(build_data())
    def test_login(self, mobile, pwd, status_code, success, code, message):
        # 测试数据
        # mobile = "13800000002"
        # pwd = "123456"

        # 发送请求
        r = self.login_api.login(mobile, pwd)
        logging.info("json data={}".format(r.json()))

        # 断言
        utils.common_assert(self, r, status_code, success, code, message)

        # 获取token数据，并保存
        if success:
            json_data = r.json()
            token = json_data.get("data")
            app.headers_data["Authorization"] = "Bearer " + token

    # 登录成功
    def atest01_login_success(self):
        # 测试数据
        mobile = "13800000002"
        pwd = "123456"

        # 发送请求
        r = self.login_api.login(mobile, pwd)
        logging.info("json data={}".format(r.json()))

        # 断言
        utils.common_assert(self, r, 200, True, 10000, "操作成功")

        # 获取token数据，并保存
        json_data = r.json()
        token = json_data.get("data")
        app.headers_data["Authorization"] = "Bearer " + token

        # self.assertEqual(200, r.status_code)
        # json_data = r.json()
        # self.assertTrue(json_data.get("success"))
        # self.assertEqual(True, json_data.get("success"))
        # self.assertEqual(10000, json_data.get("code"))
        # self.assertIn("操作成功", json_data.get("message"))

    # 用户名不存在
    def atest02_login_username_is_not_exist(self):
        # 测试数据
        mobile = "13888889999"
        pwd = "123456"

        # 发送请求
        r = self.login_api.login(mobile, pwd)

        # 断言
        utils.common_assert(self, r, 200, False, 20001, "用户名或密码错误")

        # self.assertEqual(200, r.status_code)
        json_data = r.json()
        # logging.info("json_data===" + str(json_data))
        # logging.info("json_data===%s" % json_data)
        logging.info("json_data==={} {}".format(r.json(), "hello"))
        # logging.info("json_data=%s %s", json_data, "hello")
        # self.assertFalse(json_data.get("success"))
        # self.assertEqual(False, json_data.get("success"))
        # self.assertEqual(20001, json_data.get("code"))
        # self.assertIn("用户名或密码错误", json_data.get("message"))

    # 密码错误
    def atest03_login_pwd_error(self):
        # 测试数据
        mobile = "13800000002"
        pwd = "error"

        # 发送请求
        r = self.login_api.login(mobile, pwd)

        # 断言
        utils.common_assert(self, r, 200, False, 20001, "用户名或密码错误")

    # 用户名为空
    def atest04_login_username_is_empty(self):
        # 测试数据
        mobile = ""
        pwd = "123456"

        # 发送请求
        r = self.login_api.login(mobile, pwd)

        # 断言
        utils.common_assert(self, r, 200, False, 20001, "用户名或密码错误")

    # 密码为空
    def atest05_login_pwd_is_empty(self):
        # 测试数据
        mobile = "13800000002"
        pwd = ""

        # 发送请求
        r = self.login_api.login(mobile, pwd)

        # 断言
        utils.common_assert(self, r, 200, False, 20001, "用户名或密码错误")

        # 密码为空

    # 缺少用户名参数
    def atest06_login_no_username(self):
        # 测试数据
        mobile = None
        pwd = "123456"

        # 发送请求
        r = self.login_api.login(mobile, pwd)

        # 断言
        utils.common_assert(self, r, 200, False, 20001, "用户名或密码错误")

    # 缺少密码参数
    def atest07_login_no_pwd(self):
        # 测试数据
        mobile = "13800000002"
        pwd = None

        # 发送请求
        r = self.login_api.login(mobile, pwd)

        # 断言
        utils.common_assert(self, r, 200, False, 20001, "用户名或密码错误")

    # 无参数
    def atest08_login_no_param(self):
        # 测试数据
        mobile = None
        pwd = None

        # 发送请求
        r = self.login_api.login(mobile, pwd)

        # 断言
        utils.common_assert(self, r, 200, False, 20001, "用户名或密码错误")

        # 无参数

    def atest09_login_no_param2(self):
        # 测试数据

        # 发送请求
        r = self.login_api.login2()
        # r = self.login_api.login(None, None, True)
        print("data====", r.json())

        # 断言
        utils.common_assert(self, r, 200, False, 99999, "系统繁忙")
